require "sinatra"
require "faye/websocket"
require "thin"
require "geocoder"
require "csv"
require_relative "database"
require_relative "db_ws_listeners"
require_relative "auth"
require_relative "chat"
require_relative "websockets"
require_relative "queueing"

enable :sessions

use Rack::Logger

Faye::WebSocket.load_adapter('thin')

set :clients, {sheet: [], chat: [], lock: []}

get "/" do
  erb :home
end

get "/sheet" do
  erb :sheet
end

get "/sheet/ws" do
  if Faye::WebSocket.websocket?(env)
    ws = Faye::WebSocket.new(env, nil, {ping: 15})

    ws.on :open do |event|
      p [:open, ws.object_id]
      settings.clients[:sheet] << ws
    end

    ws.on :close do |event|
      p [:close, ws.object_id, event.code, event.reason]
      settings.clients[:sheet].delete(ws)
      ws = nil
    end

    # Return async Rack response
    ws.rack_response
  else
    redirect back
  end
end

get "/people" do
  people = []
  @people.all.each { |person| people.push(person.attributes) }
  headers("Content-Type" => "application/json",
          "Content-Disposition" => "attachment",
          "filename" => "people.json")
  body people.to_json
  status 200
end

get "/people/geojson" do
  features = []
  @people.all.map do |person|
    features.push({
      type: "feature",
      geometry: {
        type: "point",
        coordinates: [person.lat, person.lng]
      },
      properties: person.attributes
      })
  end
  headers("Content-Type" => "application/vnd.geo+json",
          "Content-Disposition" => "attachment",
          "filename" => "people.geojson")
  body [{type: "FeatureCollection", features: features}.to_json]
  status 200
end

get "/people/csv" do
  csv_string = CSV.generate do |csv|
    csv << Person.column_names
    @people.all.each do |person|
      csv << person.attributes.values
    end
  end
  headers("Content-Type" => "text/csv",
          "Content-Disposition" => "attachment",
          "filename" => "people.csv")
  body [csv_string]
  status 200
end

get "/people/nonrescued" do
  people = []
  @people.where.not(status: "Rescued").each { |person| people.push(person.attributes) }
  people.to_json
end

get "/people/rescued" do
  people = []
  @people.where(status: "Rescued").each { |person| people.push(person.attributes) }
  people.to_json
end

get "/person/json/:id" do
  Person.find(params[:id]).to_json
end

get "/person/create" do
  #@google_maps_api_key = ENV["GOOGLE_MAPS_API_KEY"]
  #@location = Geocoder.search(request.ip)[0]
  #erb :form
  redirect '/person/create/1'
end

get '/person/create/1' do
  # Broad location
  erb :new_person_1
end

get '/person/create/2' do
  # Detailed address
  @person = Person.create(lat: params[:lat], lng: params[:long])
  @latlng = "#{params[:lat]},#{params[:long]}"
  @maps_key = ENV['GOOGLE_MAPS_API_KEY']
  erb :new_person_2
end

post '/person/create/3' do
  permitted = %w[lat lng street_address city apt zip_code]
  @person = Person.find params[:person_id]
  @person.update(params.select { |k, _| permitted.include? k })
  erb :new_person_3
end

post '/person/create/4' do
  permitted = %w[name number_of_people twitter_handle medical_conditions extra_details]
  @person = Person.find params[:person_id]
  @person.update(params.select { |k, _| permitted.include? k })
  erb :new_person_4
end

get "/person/:id" do
  @id = params[:id]
  puts "ID=#{@id}"
  @person = @people.find(params[:id])
  erb :person
end

post "/person/create" do
  if params[:id].nil?
    p = Person.create(lat: params[:lat], lng: params[:lng])
  else
    p = @people.find(params[:id].to_i)
    @people.update(params[:id].to_i, params.select { |k,v| %w[lat lng name incident_number street_address apt city zip_code twitter_handle priority status number_of_people].include? k})
  end
  status 200
  body [p.id.to_s]
end

get "/person/delete/:id" do
  @people.find(params[:id]).destroy
  redirect "/"
end

post "/person/:id" do
  @people.update(params[:id], params.select { |k,v| %w[lat lng name incident_number street_address apt city zip_code twitter_handle priority status number_of_people].include? k})
  redirect "/"
end

post "/comment/create" do
  Comment.create(person_id: params[:person_id], body: params[:body])
  redirect back
end

post "/search" do
  body = JSON.parse(request.body.read)
  attribute_queries = Person.column_names.map { |a| "#{a} LIKE '%#{body[a.to_s]}%'" unless body[a.to_s].nil? || body[a.to_s].empty? }.reject(&:nil?).reject(&:empty?)
  @people.where(attribute_queries.join(' AND ')).to_json
end

get "/search" do
  erb :search
end
