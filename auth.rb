require "securerandom"
require "bcrypt"

set :whitelist, [%r{^[/]{0,1}$},
                 %r{^/sheet[/]{0,1}$},
                 %r{^/sheet/ws[/]{0,1}$},
                 %r{^/people[/]{0,1}\w*$},
                 %r{^/person/json/\d*$},
                 %r{^/login[/]{0,1}$}
               ]

def logged_in?
  return false unless sess = Session.find_by(cookie: session[:cookie])
  @user = sess.user
end

def new_user(username, pass, role_id)
  salt = BCrypt::Engine.generate_salt
  User.create(username:username, password_hash: BCrypt::Engine.hash_secret(pass, salt), password_salt: salt, role_id: role_id)
end

before do
  @people = Person.where.not(key: nil).where(key: session[:person_key])
  @queues = ReviewQueue.none
  break unless logged_in?
  # if @user.role_id == 0
  #   @people = Person.where(key: session[:person_key])
  if @user.role_id == 1
    @queues = @user.queues
  elsif @user.role_id == 2
    @people = Person.where.not(status: "Rescued")#.map { |person| Person.new(person.attributes) }
  elsif @user.role_id == 3
    @queues = @user.queues
  end
  # unless logged_in? || settings.whitelist.any? { |url| url =~ request.path_info }
  #   redirect "/login"
  # end
end

get "/login" do
  redirect (back.to_s.end_with?("/login") ? request.base_url : back) if logged_in?
  erb :login
end

get "/logout" do
  if logged_in?
    Session.find_by(cookie: session[:cookie]).destroy
    session.clear
  end
  redirect back
end

post "/login" do
  if !logged_in? && (usr = User.find_by(username: params[:username])) && usr.password_hash == BCrypt::Engine.hash_secret(params[:password], usr.password_salt)
    session.clear
    session[:cookie] = SecureRandom.base64
    usr.sessions.create(cookie:session[:cookie])
  end
  redirect (back.to_s.end_with?("/login") ? request.base_url : back)
end