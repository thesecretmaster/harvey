class Auth < ActiveRecord::Migration[5.1]
  def change
    add_column :comments, :user_id, :integer
  	create_table :users do |i|
  	  i.text :name
      i.text :username
      i.text :password_hash
      i.text :password_salt
      i.text :oauth_token
      i.text :oauth_provider
  	end
    create_table :sessions do |i|
      i.text :cookie
      i.integer :user_id
      i.datetime :created_at
      i.datetime :updated_at
    end
  end
end
