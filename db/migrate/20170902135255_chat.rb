class Chat < ActiveRecord::Migration[5.1]
  def change
    create_table :channels do |i|
      i.text :name
      i.text :discription
      i.boolean :public
      i.datetime :created_at
    end
    create_table :user_channels do |i|
      i.integer :user_id
      i.integer :channel_id
      i.datetime :last_read # Also serves as joined bool
    end
    create_table :messages do |i|
      i.integer :user_id
      i.integer :channel_id
      i.datetime :created_at
      i.text :body
    end
  end
end
