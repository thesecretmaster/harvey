class Roles < ActiveRecord::Migration[5.1]
  def change
    add_column :people, :key, :text
    add_column :users, :role_id, :integer

    create_table :user_queues do |i|
      i.integer :user_id
      i.integer :queue_id
      i.boolean :write
    end
  end
end
