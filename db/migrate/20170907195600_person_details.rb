class PersonDetails < ActiveRecord::Migration[5.1]
  def change
    add_column :people, :medical_conditions, :text
    add_column :people, :extra_details, :text
  end
end