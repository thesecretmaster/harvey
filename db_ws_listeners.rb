class Person < ActiveRecord::Base
  after_update do
    Sinatra::Application.settings.clients[:sheet].each do |client|
      client.send({rec: self, meta: 0}.to_json)
    end
  end

  after_destroy do
    Sinatra::Application.settings.clients[:sheet].each do |client|
      client.send({rec: self.id, meta: 1}.to_json)
    end
  end

  after_create do
    Sinatra::Application.settings.clients[:sheet].each do |client|
      client.send({rec: self, meta: 2}.to_json)
    end
  end
end

class Message < ActiveRecord::Base
  after_create do
    Sinatra::Application.settings.clients[:chat].each do |client|
      client.send({message: self, channel: self.channel, user: self.user}.to_json)
    end
  end
end