require_relative "database"
require "csv"

# TODO: Progressbar
CSV.foreach("./data.csv", headers: true) do |row|
  person = Person.create(lat: row["Latitude Coordinate"],
                        lng: row["Longitude Coordinate"],
                        incident_number: row["Incident Number"],
                        name: row["Name"],
                        street_address: row["Street Address"],
                        apt: row["Apt #"],
                        city: row["City"],
                        zip_code: row["Zip Code"],
                        twitter_handle: row["Twitter Handle"],
                        priority: row["Priority"],
                        status: row["Status"],
                        number_of_people: row["Number of People"])
  puts "making new person with lat #{person.lat} and long #{person.lng}, #{person}"
end